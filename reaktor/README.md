# The Gloves Project - Reaktor Bridge
-----------------------------------------

Receives all gloves data as OSC messages and prints them out. Adapt to use the gloves data for your own project.

Includes a Macro for receiving data from The Gloves and an example ensemble.