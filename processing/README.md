# The Gloves Project - Processing Bridge
-----------------------------------------

Receives all gloves data as OSC messages and prints them out. Adapt to use the gloves data for your own project.

Requires:
---------

* oscP5 library (http://www.sojamo.de/libraries/oscP5/)