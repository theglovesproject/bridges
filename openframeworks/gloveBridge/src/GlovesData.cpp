//
//  GlovesData.cpp
//  gloveBridge
//
//  Created by Adam Stark on 28/01/2013.
//
//

#include "GlovesData.h"

GlovesData :: GlovesData()
{
    receiver.setup(8080);
    
    font.loadFont("verdana.ttf", 25, true, true);
    font.setLineHeight(34.0f);
    font.setLetterSpacing(1.035);
}

void GlovesData :: update()
{
    // check for waiting messages
	while(receiver.hasWaitingMessages()){
		// get the next message
		ofxOscMessage m;
		receiver.getNextMessage(&m);
        
    
		//================== ORIENTATION ==================
        //========= hand orientation in 3 dimensions ======
        
        //-------------------------------------------------
        // /glover/pitchL - left hand pitch
        // range: [-90,90]
        //
        
		if(m.getAddress() == "/glover/orientationL"){
			
			pitchL = m.getArgAsFloat(0);
            rollL = m.getArgAsFloat(1);
            yawL = m.getArgAsFloat(2);	
		}
        
        //-------------------------------------------------
        // /glover/pitchR - right hand pitch
        // range: [-90,90]
        //
        if (m.getAddress() == "/glover/orientationR")
        {
            pitchR = m.getArgAsFloat(0);
            rollR = m.getArgAsFloat(1);
            yawR = m.getArgAsFloat(2);
        }
        
    
        //================== AVERAGE FLEX =================
        //=========== finger flex: open hand to fist ======
        
        //-------------------------------------------------
        // /glover/averageFlexL - left hand average flex
        // range: [0,1]
        //
        if (m.getAddress() == "/glover/averageFlexL")
        {
            averageFlexL = m.getArgAsFloat(0);
        }
        
        
        //-------------------------------------------------
        // /glover/averageFlexR - right hand average flex
        // range: [0,1]
        //
        if (m.getAddress() == "/glover/averageFlexR")
        {
            averageFlexR = m.getArgAsFloat(0);
        }
        
        
        
        //=================== DRUM HITS ===================
        //============== drum hitting gestures ============
        
        //-------------------------------------------------
        // /glover/drumL - left hand drum hit
        // event
        //
        if (m.getAddress() == "/glover/drumL")
        {
            drumL = true;
        }
        
        //-------------------------------------------------
        // /glover/drumR - right hand drum hit
        // event
        //
        if (m.getAddress() == "/glover/drumR")
        {
            drumR = true;
        }
        
        
        //==================== DIRECTION ==================
        //======== number indicating hand direction =======
        
        //-------------------------------------------------
        // /glover/directionL - direction of left hand
        // values:
        // 0 forward
        // 1 backward
        // 2 left
        // 3 right
        // 4 down
        // 5 up
        //
        if (m.getAddress() == "/glover/directionL")
        {
            directionL = m.getArgAsInt32(0);
        }
        
        
        //-------------------------------------------------
        // /glover/directionR - direction of right hand
        // values:
        // 0 forward
        // 1 backward
        // 2 left
        // 3 right
        // 4 down
        // 5 up
        //
        if (m.getAddress() == "/glover/directionR")
        {
            directionR = m.getArgAsInt32(0);
        }
        
        
        //===================== POSTURE ===================
        //============= the current hand posture ==========
        
        //-------------------------------------------------
        // /glover/postureL - the current left hand posture
        // values:
        // -1 no posture
        // 0 fist
        // 1 puppet hand
        // 2 open hand
        // 3 one finger point
        // 4 secret thumb
        //
        if (m.getAddress() == "/glover/postureL")
        {
            postureL = m.getArgAsInt32(0);
        }
        
        
        //-------------------------------------------------
        // /glover/postureR - the current right hand posture
        // values:
        // -1 no posture
        // 0 fist
        // 1 puppet hand
        // 2 open hand
        // 3 one finger point
        // 4 secret thumb
        //
        if (m.getAddress() == "/glover/postureR")
        {
            postureR = m.getArgAsInt32(0);
        }
        
        
        
        
        //=============== HORIZONTAL GYRO PEAKS ===========
        //============= horizontal gyroscope peaks  =======
        
        //-------------------------------------------------
        // /glover/gyroPeakXL - left hand horizontal gyro peak
        // event
        //
        if (m.getAddress() == "/glover/gyroPeakXL")
        {
            gyroPeakXL = true;
        }
        
        
        //-------------------------------------------------
        // /glover/gyroPeakXR - right hand horizontal gyro peak
        // event
        //
        if (m.getAddress() == "/glover/gyroPeakXR")
        {
            gyroPeakXR = true;
        }
        
        
        //================ VERTICAL GYRO PEAKS ============
        //============== vertical gyroscope peaks  ========
        
        //-------------------------------------------------
        // /glover/gyroPeakYL - left hand vertical gyro peak
        // event
        //
        if (m.getAddress() == "/glover/gyroPeakYL")
        {
            gyroPeakYL = true;
        }
        
        
        //-------------------------------------------------
        // /glover/gyroPeakYR - right hand vertical gyro peak
        // event
        //
        if (m.getAddress() == "/glover/gyroPeakYR")
        {
            gyroPeakYR = true;
        }
        
        
        //================ WRIST GYRO PEAKS ===============
        //=========== wrist flick gyroscope peaks  ========
        
        //-------------------------------------------------
        // /glover/gyroPeakZL - left wrist flick gyro peak
        // event
        //
        if (m.getAddress() == "/glover/gyroPeakZL")
        {
            gyroPeakZL = true;
        }
        
        
        //-------------------------------------------------
        // /glover/gyroPeakZR - right wrist flick gyro peak
        // event
        //
        if (m.getAddress() == "/glover/gyroPeakZR")
        {
            gyroPeakZR = true;
        }
        
        
        //=================== WRIST ANGLE =================
        //============= the angle between wrists ==========
        
        //-------------------------------------------------
        // /glover/wristAngle - the angle between wrists
        // range: [0,180]
        //
        if (m.getAddress() == "/glover/wristAngle")
        {
            wristAngle = m.getArgAsFloat(0);
        }
        
	}
}

void GlovesData :: printDataToScreen()
{
    ofSetColor(255, 255, 255);
    
    font.drawString("The Gloves Project - OpenFrameworks Bridge", 20, 50);
    
    int textLeft = 20;
    int textRight = 500;
    int valLeft = 350;
    int valRight = 830;
    
    // pitch
    font.drawString("pitchL: ", textLeft, 150);
    font.drawString(ofToString(pitchL, 2), valLeft, 150);
    font.drawString("pitchR: ", textRight, 150);
    font.drawString(ofToString(pitchR, 2), valRight, 150);
    
    // yaw
    font.drawString("yawL: ", textLeft, 200);
    font.drawString(ofToString(yawL, 2), valLeft, 200);
    font.drawString("yawR: ", textRight, 200);
    font.drawString(ofToString(yawR, 2), valRight, 200);
    
    // roll
    font.drawString("rollL: ", textLeft, 250);
    font.drawString(ofToString(rollL, 2), valLeft, 250);
    font.drawString("rollR: ", textRight, 250);
    font.drawString(ofToString(rollR, 2), valRight, 250);
    
    // average flex
    font.drawString("averageFlexL: ", textLeft, 300);
    font.drawString(ofToString(averageFlexL, 2), valLeft, 300);
    font.drawString("averageFlexR: ", textRight, 300);
    font.drawString(ofToString(averageFlexR, 2), valRight, 300);
    
    // drum hits
    font.drawString("drumL: ", textLeft, 350);
    if (drumL)
    {
        drumL = false;
        ofFill();
    }
    else {
        ofNoFill();
    };
    ofRect(valLeft, 350-20, 80, 20);
    
    font.drawString("drumR: ", textRight, 350);
    if (drumR)
    {
        drumR = false;
        ofFill();
    }
    else {
        ofNoFill();
    }
    ofRect(valRight, 350-20, 80, 20);
    
    
    // direction
    font.drawString("directionL: ", textLeft, 400);
    font.drawString(ofToString(directionL), valLeft, 400);
    font.drawString("directionR: ", textRight, 400);
    font.drawString(ofToString(directionR), valRight, 400);
    
    // posture
    font.drawString("postureL: ", textLeft, 450);
    font.drawString(ofToString(postureL), valLeft, 450);
    font.drawString("postureR: ", textRight, 450);
    font.drawString(ofToString(postureR), valRight, 450);
    
    
    // gyroPeak X
    font.drawString("gyroPeakXL: ", textLeft, 500);
    if (gyroPeakXL)
    {
        gyroPeakXL = false;
        ofFill();
    }
    else {
        ofNoFill();
    };
    ofRect(valLeft, 500-20, 80, 20);
    
    font.drawString("gyroPeakXR: ", textRight, 500);
    if (gyroPeakXR)
    {
        gyroPeakXR = false;
        ofFill();
    }
    else {
        ofNoFill();
    }
    ofRect(valRight, 500-20, 80, 20);
    
    
    // gyroPeak Y
    font.drawString("gyroPeakYL: ", textLeft, 550);
    if (gyroPeakYL)
    {
        gyroPeakYL = false;
        ofFill();
    }
    else {
        ofNoFill();
    };
    ofRect(valLeft, 550-20, 80, 20);
    
    font.drawString("gyroPeakYR: ", textRight, 550);
    if (gyroPeakYR)
    {
        gyroPeakYR = false;
        ofFill();
    }
    else {
        ofNoFill();
    }
    ofRect(valRight, 550-20, 80, 20);
    
    
    
    // gyroPeak Z
    font.drawString("gyroPeakZL: ", textLeft, 600);
    if (gyroPeakZL)
    {
        gyroPeakZL = false;
        ofFill();
    }
    else {
        ofNoFill();
    };
    ofRect(valLeft, 600-20, 80, 20);
    
    font.drawString("gyroPeakZR: ", textRight, 600);
    if (gyroPeakZR)
    {
        gyroPeakZR = false;
        ofFill();
    }
    else {
        ofNoFill();
    }
    ofRect(valRight, 600-20, 80, 20);
    
    
    
    // wristAngle
    font.drawString("wristAngle: " + ofToString(wristAngle,2), 360, 700);
}
