#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    
	ofBackground(102, 102, 102);
}

//--------------------------------------------------------------
void testApp::update(){
    
    // tell gloves object to update osc messages
    gloves.update();
}


//--------------------------------------------------------------
void testApp::draw(){
    
    // uncomment this to get a screen print out of all values
	gloves.printDataToScreen();
    
    // example of drawing a rectangle
    ofRect(gloves.pitchL,gloves.pitchR,gloves.averageFlexL*300,gloves.averageFlexL*300);
    
    
    // a full list of messages is...
    //    float pitchL, pitchR;
    //    float yawL, yawR;
//    float rollL, rollR;
//    int drumRegionL, drumRegionR;
//    float drumIntensityL, drumIntensityR;
//    int directionL, directionR;
//    int postureL, postureR;
//    bool gyroPeakXL, gyroPeakXR;
//    bool gyroPeakYL, gyroPeakYR;
//    bool gyroPeakZL, gyroPeakZR;
//    bool drumL, drumR;
//    float magnitudeL, magnitudeR;
//    float flexL[6];
//    float flexR[6];
//    float averageFlexL, averageFlexR;
//    float wristAngle;
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}