//
//  GlovesData.h
//  gloveBridge
//
//  Created by Adam Stark on 28/01/2013.
//
//

#ifndef __gloveBridge__GlovesData__
#define __gloveBridge__GlovesData__

#include <iostream>
#include "ofxOsc.h"

class GlovesData {
public:
    
    GlovesData();
    void update();
    void printDataToScreen();
    
    //==================================================
    // Declare glove variables
    float pitchL, pitchR;
    float yawL, yawR;
    float rollL, rollR;
    int drumRegionL, drumRegionR;
    float drumIntensityL, drumIntensityR;
    int directionL, directionR;
    int postureL, postureR;
    bool gyroPeakXL, gyroPeakXR;
    bool gyroPeakYL, gyroPeakYR;
    bool gyroPeakZL, gyroPeakZR;
    bool drumL, drumR;
    float magnitudeL, magnitudeR;
    float flexL[6];
    float flexR[6];
    float averageFlexL, averageFlexR;
    float wristAngle;
    ofxOscReceiver receiver;
    
    ofTrueTypeFont font;


    
    
};

#endif /* defined(__gloveBridge__GlovesData__) */
