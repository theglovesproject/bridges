## The Gloves Project - Bridges

This repository contains bridges that allow various applications to receive and interpret data from The Gloves.

We have built bridges for:

* Max
* OpenFrameworks
* Processing
* PureData
* Python
* Reaktor
* SuperCollider

In future we hope to support:

* Cinder
* Chuck
* vvvv
* iOS
* Android
* Osculator

## OSC Dictionary

Receive on port 8080 send on port 8000

# Recieving
* /glover/orientationL, f, f, f 
* /glover/orientationR, f, f, f

where f, f, f = pitch[-180:180], roll[-180:180], yaw[-180:180]

* /glover/wristAngle, f

where f = angle[0:180]

* /glover/postureL, i
* /glover/postureR, i

where i =	-1 	no posture
    			0 	fist
    			1 	puppet hand
    			2 	open hand
    			3 	1 finger point
    			4 	secret thumb

* /glover/flexL, f, f, f, f, f, f
* /glover/flexR, f, f, f, f, f, f

where f, f, f, f, f, f = thumb, indexLower, indexUpper, middleLower, middleUpper, ring
all [0:1]

* /glover/magnitudeL, f
* /glover/magnitudeR, f

where f [0:12]?

* /glover/gyroPeakXL
* /glover/gyroPeakYL
* /glover/gyroPeakZL

* /glover/gyroPeakXR
* /glover/gyroPeakYR
* /glover/gyroPeakZR

* /glover/drumL, i, f
* /glover/drumR, i, f

where i = ? f = velocity [0:1]

* /directionL
* /directionR
    0 forward
    1 backward
    2 left
    3 right
    4 down
    5 up

# Sending

* /glover/leds/, i, i, i, i

where i, i, i, i = hand (0 = left), (1 = right), red (0 - 255), green (0 - 255), blue (0 - 255)
