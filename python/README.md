# The Gloves Project - Python Bridge
-----------------------------------------

Receives all gloves data as OSC messages and prints them out. Adapt to use the gloves data for your own project.

Requirements
------------

* pyOSC (https://trac.v2.nl/wiki/pyOSC)


How To Run
----------

To run, open the Terminal at the directory containing the scripy and run:

> python TheGlovesProject.py