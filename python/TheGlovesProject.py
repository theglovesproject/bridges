#===============================================================
#
# THE GLOVES PROJECT - PYTHON BRIDGE
#
# Receives and prints out all glove messages
# Adapt to use the gloves with your own code
#
# Requires: pyOSC (https://trac.v2.nl/wiki/pyOSC)
#
#===============================================================

from OSC import OSCServer
from time import sleep
import types

# listen for messages on port 8080
server = OSCServer( ("localhost", 8080) )

server.timeout = 0
run = True

def handle_timeout(self):
    self.timed_out = True


# add method to handle timeouts
server.handle_timeout = types.MethodType(handle_timeout, server)


#================ ORIENTATION ====================
#======= hand orientation in 3 dimensions ========

#-------------------------------------------------
# /glover/orientationL - left hand orientation
def gloveMessage_orientationL(path, tags, args, source):
    
    pitchL = args[0]
    rollL = args[1]
    yawL = args[2]
    print path + " : " + str(pitchL) + ", " + str(rollL) + ", " + str(yawL)

#-------------------------------------------------
# /glover/orientationR - right hand orientation
def gloveMessage_orientationR(path, tags, args, source):

    pitchR = args[0]
    rollR = args[1]
    yawR = args[2]
    print path + " : " + str(pitchR) + ", " + str(rollR) + ", " + str(yawR)



#================== AVERAGE FLEX =================
#=========== finger flex: open hand to fist ======

#-------------------------------------------------
# /glover/averageFlexL - left hand average flex
# range: [0,1]
#
def gloveMessage_averageFlexL(path, tags, args, source):

    print path + " : " + str(args[0])


#-------------------------------------------------
# /glover/averageFlexR - right hand average flex
# range: [0,1]
#
def gloveMessage_averageFlexR(path, tags, args, source):

    print path + " : " + str(args[0])


#======================= FLEX ====================
#======== flex values for individual sensors =====

#-------------------------------------------------
# /glover/flexL - left hand flex values
# six different values:
# [thumb, indexLower, indexUpper, middleLower, middleUpper, ring]
# range: [0,1]
#
def gloveMessage_flexL(path, tags, args, source):

    thumbL = args[0]
    indexLowerL = args[1]
    indexUpperL = args[2]
    middleLowerL = args[3]
    middleUpperL = args[4]
    ringL = args[5]
    print path + " : " + str([thumbL,indexLowerL,indexUpperL,middleLowerL,middleUpperL,ringL])


#-------------------------------------------------
# /glover/flexR - right hand flex values
# six different values:
# [thumb, indexLower, indexUpper, middleLower, middleUpper, ring]
# range: [0,1]
#
def gloveMessage_flexR(path, tags, args, source):

    thumbR = args[0]
    indexLowerR = args[1]
    indexUpperR = args[2]
    middleLowerR = args[3]
    middleUpperR = args[4]
    ringR = args[5]
    print path + " : " + str([thumbR,indexLowerR,indexUpperR,middleLowerR,middleUpperR,ringR])



#================== MAGNITUDE =================
#============= magnitude of movement ==========

#-------------------------------------------------
# /glover/magnitudeL - left hand magnitude
# range: [0,12]
#
def gloveMessage_magnitudeL(path, tags, args, source):

    print path + " : " + str(args[0])


#-------------------------------------------------
# /glover/magnitudeR - right hand magnitude
# range: [0,1]
#
def gloveMessage_magnitudeR(path, tags, args, source):

    print path + " : " + str(args[0])



#=================== DRUM HITS ===================
#============== drum hitting gestures ============

#-------------------------------------------------
# /glover/drumL - left hand drum hit
# event, with associated region and intensity
#
def gloveMessage_drumL(path, tags, args, source):

    drumRegionL = args[0]
    drumIntensityL = args[1]

    print path + " : " + str([drumRegionL,drumIntensityL])


#-------------------------------------------------
# /glover/drumR - right hand drum hit
# event, with associated region and intensity
#
def gloveMessage_drumR(path, tags, args, source):

    drumRegionR = args[0]
    drumIntensityR = args[1]

    print path + " : " + str([drumRegionR,drumIntensityR])


#==================== DIRECTION ==================
#======== number indicating hand direction =======

#-------------------------------------------------
# /glover/directionL - direction of left hand
# values:
# 0 forward
# 1 backward
# 2 left
# 3 right
# 4 down
# 5 up
#
def gloveMessage_directionL(path, tags, args, source):

    print path + " : " + str(args[0])


#-------------------------------------------------
# /glover/directionR - direction of right hand
# values:
# 0 forward
# 1 backward
# 2 left
# 3 right
# 4 down
# 5 up
#
def gloveMessage_directionR(path, tags, args, source):

    print path + " : " + str(args[0])




#===================== POSTURE ===================
#============= the current hand posture ==========

#-------------------------------------------------
# /glover/postureL - the current left hand posture
# values:
# -1 no posture
# 0 fist
# 1 puppet hand
# 2 open hand
# 3 one finger point
# 4 secret thumb
#
def gloveMessage_postureL(path, tags, args, source):

    print path + " : " + str(args[0])


#-------------------------------------------------
# /glover/postureR - the current right hand posture
# values:
# -1 no posture
# 0 fist
# 1 puppet hand
# 2 open hand
# 3 one finger point
# 4 secret thumb
#
def gloveMessage_postureR(path, tags, args, source):

    print path + " : " + str(args[0])



#=============== HORIZONTAL GYRO PEAKS ===========
#============= horizontal gyroscope peaks  =======

#-------------------------------------------------
# /glover/gyroPeakXL - left hand horizontal gyro peak
# event
#
def gloveMessage_gyroPeakXL(path, tags, args, source):

    print path


#-------------------------------------------------
# /glover/gyroPeakXR - right hand horizontal gyro peak
# event
#
def gloveMessage_gyroPeakXR(path, tags, args, source):

    print path



#================ VERTICAL GYRO PEAKS ============
#============== vertical gyroscope peaks  ========

#-------------------------------------------------
# /glover/gyroPeakYL - left hand vertical gyro peak
# event
#
def gloveMessage_gyroPeakYL(path, tags, args, source):

    print path


#-------------------------------------------------
# /glover/gyroPeakYR - right hand vertical gyro peak
# event
#
def gloveMessage_gyroPeakYR(path, tags, args, source):

    print path




#================ WRIST GYRO PEAKS ===============
#=========== wrist flick gyroscope peaks  ========

#-------------------------------------------------
# /glover/gyroPeakZL - left wrist flick gyro peak
# event
#
def gloveMessage_gyroPeakZL(path, tags, args, source):

    print path


#-------------------------------------------------
# /glover/gyroPeakZR - right wrist flick gyro peak
# event
#
def gloveMessage_gyroPeakZR(path, tags, args, source):

    print path


#=================== WRIST ANGLE =================
#============= the angle between wrists ==========

#-------------------------------------------------
# /glover/wristAngle - the angle between wrists
# range: [0,180]
#
def gloveMessage_wristAngle(path, tags, args, source):

    print path + " : " + str(args[0])



#==================================================
# match callback functions to messages
server.addMsgHandler( "/glover/orientationL", gloveMessage_orientationL)
server.addMsgHandler( "/glover/orientationR", gloveMessage_orientationR)
server.addMsgHandler( "/glover/flexL", gloveMessage_flexL)
server.addMsgHandler( "/glover/flexR", gloveMessage_flexR)
server.addMsgHandler( "/glover/averageFlexL", gloveMessage_averageFlexL)
server.addMsgHandler( "/glover/averageFlexR", gloveMessage_averageFlexR)
server.addMsgHandler( "/glover/drumL", gloveMessage_drumL)
server.addMsgHandler( "/glover/drumR", gloveMessage_drumR)
server.addMsgHandler( "/glover/directionL", gloveMessage_directionL)
server.addMsgHandler( "/glover/directionR", gloveMessage_directionR)
server.addMsgHandler( "/glover/magnitudeL", gloveMessage_magnitudeL)
server.addMsgHandler( "/glover/magnitudeR", gloveMessage_magnitudeR)
server.addMsgHandler( "/glover/postureL", gloveMessage_postureL)
server.addMsgHandler( "/glover/postureR", gloveMessage_postureR)
server.addMsgHandler( "/glover/gyroPeakXL", gloveMessage_gyroPeakXL)
server.addMsgHandler( "/glover/gyroPeakXR", gloveMessage_gyroPeakXR)
server.addMsgHandler( "/glover/gyroPeakYL", gloveMessage_gyroPeakYL)
server.addMsgHandler( "/glover/gyroPeakYR", gloveMessage_gyroPeakYR)
server.addMsgHandler( "/glover/gyroPeakZL", gloveMessage_gyroPeakZL)
server.addMsgHandler( "/glover/gyroPeakZR", gloveMessage_gyroPeakZR)
server.addMsgHandler( "/glover/wristAngle", gloveMessage_wristAngle)





#==================================================
# run server to listen for messages
if (run):
    print " "
    print "listening for messages..."
    print " "

while run:
    
    sleep(0.05)
    
    server.timed_out = False
    
    # handle all pending requests then return
    while not server.timed_out:
        server.handle_request()

# when done, close the server
server.close()